
This is a collection of scripts I like to use as a starting point when I sit 
down to make a game. I don't claim to understand the legality of putting all 
of these files in one place and giving them a common namespace, but since 
Editor extensions aren't compiled with your game's binary issues with copyleft 
software may not affect your project. Not all files share the same license.
Check the comments at the top of those files to know. Furthermore, it's likely 
that many of the authors of the files posted on wiki.unity3d.com were not 
aware that content is automatically licensed under Creative Commons 
Attribution Share Alike. The author of MaxRectsBinPack, for example, clearly 
states that the work is public domain and you can "do whatever you want with 
it."

Edit: May 4, 2014
I was reading into the license thing again and I came across http://wiki.
unity3d.com/index.php/Talk:Main_Page#Terms_of_Use. It seems as if the CC3 
Share Alike license was tacked on after many of these scripts were posted to 
Unify. On July 27, 2011, Technicat pointed out that there was no terms of use.
His assumption was that code was licensed under MIT. On September 2, 2011,
NCarter, author of many of the tools in this repo, replied with a dead link to 
Unify Community's copyrights page and the assertion that "everything on the 
wiki may be freely used in any way you can imagine." I'm leaning toward 
removing the CC3 Share Alike license at this point as it seems to be a blanket 
stamp on all code posted to the Unify Community without the consent of the 
authors. In many cases, it was tacked on years after the code was posted. To 
be safe though, I'll wait until I have talked to the authors to remove it.